module Main where
import           Control.Parallel            (par)
import           Control.Parallel.Strategies (parMap, rseq)
import           Data.Algorithm.Diff
import           Data.Maybe                  (fromMaybe)
import           Data.Wakfu.Inspect
import qualified Data.Yaml                   as Yaml
import           GHC.Exts                    (sortWith)
import           System.Console.ANSI
import           System.Environment          (getArgs)
import           System.FilePath             (takeExtension)

data Mode
    = Diff FilePath FilePath
    | Dump FilePath FilePath

data ProtocolDiff
    = AddedMessage Int String [Diff MemberType]
    | RemovedMessage Int String
    | ChangedMessage Int String String [Diff MemberType]
    | Noop

main :: IO ()
main = parseArgs <$> getArgs >>= work

parseArgs :: [String] -> Mode
parseArgs ["-dump", jar, out] =
    Dump jar out
parseArgs ["-diff", src, dst] =
    Diff src dst
parseArgs _ =
    error $ unlines
        [ "expecting one of the following argument sets:"
        , "-dump [path to an input wakfu binaries JAR file] [path to an output YAML file]"
        , "-diff [path to an input source Wakfu binaries JAR or a YAML file] [path to an input destination Wakfu binaries JAR or a YAML file]"]

loadProtocol :: FilePath -> IO ([Message], [Message])
loadProtocol path =
    let ext = takeExtension path in
    if ext == ".jar" then inspectMessageStructures path
    else if ext == ".yaml" then
        fromMaybe (error "failed to parse the protocol file definition") <$>
            Yaml.decodeFile path
    else error "invalid file, expecting JAR or YAML"

work :: Mode -> IO ()
work (Diff src dst) = do
    putStrLn $ "loading source protocol definition from " ++ src
    (oldInputs, oldOutputs) <- loadProtocol src
    putStrLn $ "loading destination protocol definition from " ++ dst
    (newInputs, newOutputs) <- loadProtocol dst
    setSGR [SetUnderlining SingleUnderline]
    putStrLn ""
    putStrLn "input message diff"
    setSGR [SetUnderlining NoUnderline]
    mapM_ printDiff $ protocolDiff oldInputs newInputs
    setSGR [SetUnderlining SingleUnderline]
    putStrLn ""
    putStrLn "output message diff"
    setSGR [SetUnderlining NoUnderline]
    mapM_ printDiff $ protocolDiff oldOutputs newOutputs
work (Dump jar out) =
    if takeExtension jar /= ".jar" then
        error "expecting a JAR file"
    else Yaml.encodeFile out =<< inspectMessageStructures jar

printDiff :: ProtocolDiff -> IO ()
printDiff (AddedMessage mid name diffs) = do
    putStrLn ""
    putStrLn $ "addition of message with ID " ++ show mid
    putStrLn $ "[class: " ++ name ++ "]"
    mapM_ printMemberDiff diffs
printDiff (RemovedMessage mid name) = do
    putStrLn ""
    putStrLn $ "removal  of message with ID " ++ show mid
    putStrLn $ "[class: " ++ name ++ "]"
printDiff (ChangedMessage mid fstName sndName diffs) = do
    putStrLn ""
    putStrLn $ "diff on message with ID " ++ show mid
    putStrLn $ "[class in source: " ++ fstName ++
        ", destination: " ++ sndName ++ "]"
    mapM_ printMemberDiff diffs
printDiff Noop = return ()

printMemberDiff :: Diff MemberType -> IO ()
printMemberDiff (First v) = do
    setSGR [SetColor Foreground Vivid Red]
    putStrLn $ "- " ++ show v
    setSGR [Reset]
printMemberDiff (Second v) = do
    setSGR [SetColor Foreground Vivid Green]
    putStrLn $ "+ " ++ show v
    setSGR [Reset]
printMemberDiff (Both v _) = do
    setSGR [SetColor Foreground Dull White]
    print v
    setSGR [Reset]

protocolDiff :: [Message] -> [Message] -> [ProtocolDiff]
protocolDiff old new =
    let sortedOld = sortWith messageId old
        sortedNew = sortedOld `par` sortWith messageId new
    in parMap rseq buildDiff $
        getDiffBy (\a b -> messageId a == messageId b) sortedOld sortedNew
    where
        buildDiff :: Diff Message -> ProtocolDiff
        buildDiff (First msg) =
            RemovedMessage (messageId msg) $ obfuscatedName msg
        buildDiff (Second msg) =
            AddedMessage (messageId msg) (obfuscatedName msg) $
                (Second . memberType) <$> dataMembers msg
        buildDiff (Both first second) =
            let changes = messageTypesDiff first second in
            if anyChange changes then
                ChangedMessage (messageId first) (obfuscatedName first) (obfuscatedName second) changes
            else Noop
        messageTypesDiff :: Message -> Message -> [Diff MemberType]
        messageTypesDiff a b =
            let ams = memberType <$> dataMembers a
                bms = memberType <$> dataMembers b
            in getDiff ams bms
        anyChange :: [Diff a] -> Bool
        anyChange (Both _ _:xs) = anyChange xs
        anyChange (_:_)         = True
        anyChange []            = False
